export class CustomError extends Error {
  statusCode: number

  constructor(statusCode = 500, ...params: any) {
    super(...params)

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, CustomError)
    }

    this.name = 'CustomError'
    this.statusCode = statusCode
  }
}