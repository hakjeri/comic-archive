import Link from "next/link"

export const Thumbnail = ({alt, href, img, publicationDate, title}: ThumbnailProps) => {

  return (
    <div className="grid m-3 p-1 justify-items-center bg-white scale-90 transition transform hover:scale-100">
      <Link href={href}>
        <a className="w-full">
          <h2>{title}</h2>
          <img className="object-contain h-32 max-h-32" alt={alt} src={img} />
        </a>
      </Link>
    </div>
  )
}

interface ThumbnailProps {
  alt: string,
  href: string,
  img: string,
  publicationDate: Date,
  title: string,
}