import React, { useEffect, useState } from "react";
import { Thumbnail } from "../../components/Thumbnail";

const PAGINATION_AMOUNT = 12

const ComicArchive = ({ initialStrips }: ComicArchiveProps) => {
  const [stripIndex, setStripIndex] = useState<number>(initialStrips[0].num)
  const [strips, setStrips] = useState<Strip[]>([])

  useEffect(() => {
    if (stripIndex) {
      getStrips(stripIndex).then(newStrips => {
        setStrips([...strips, ...newStrips])
      })
    }
  }, [stripIndex])
  
  return (
    <>
      <h1>This is the XKCD comic archive</h1>
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4">
        {strips.map(({alt, img, num, publicationDate, title}) =>
          <Thumbnail 
            key={num}
            alt={alt}
            href={`/comics/${num}`}
            img={img}
            publicationDate={publicationDate}
            title={title}
          />
        )}
        {
          stripIndex > 0 &&
          <button className="m-3 p-2 md:col-span-2 lg:col-start-2 rounded bg-gray-500" onClick={() => setStripIndex(Math.max(stripIndex-PAGINATION_AMOUNT, 0))}>Load more</button>
        }
      </div>
    </>
  );
}

ComicArchive.getInitialProps = async () => {
  const initialStrips = await getStrips();
  return { initialStrips }
}

const getStrips = async (startIndex?: number) => {
  const response = await fetch(`${process.env.NEXT_PUBLIC_API_ORIGIN}/api/comics${startIndex && startIndex > 0 ? `?index=${startIndex}` : ""}`)
  const strips = await response.json();
  return strips
}

interface ComicArchiveProps {
  initialStrips: Strip[],
}

interface Strip {
  publicationDate: Date,
  alt: string,
  img: string,
  title: string,
  num: number,
}

export default ComicArchive;