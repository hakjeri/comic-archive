import React from "react";
import { Thumbnail } from "../../components/Thumbnail";
import { NextPageContext } from "next";
import Link from "next/link";

const Comic = ({ alt, num, img, publicationDate, title }: Strip) => {
  return (
    <div className="grid max-w-xl">
      <h1>{title}</h1>
      <div>
        <img
          alt={alt}
          src={img}
          title={title}
        />
        <p aria-hidden className="mt-2 mb-8">{alt}</p>
        <Link href="/comics">
          <a>Go back to all comics</a>
        </Link>
      </div>
    </div>
  );
}

Comic.getInitialProps = async (ctx: NextPageContext) => {
  const strip = await getStrip(ctx.query.id);
  return strip
}

const getStrip = async (comicId?: string | string[]) => {
  const response = await fetch(`${process.env.NEXT_PUBLIC_API_ORIGIN}/api/comics/${comicId}`)
  const strip = await response.json();
  return strip
}

interface Strip {
  publicationDate: Date,
  alt: string,
  img: string,
  title: string,
  num: number,
}

export default Comic;