import Link from "next/link";
import React from "react";

const StartPage = () => {
  return (
    <div>
      <h1>Welcome to the comic archive!</h1>
      <Link href="/comics">
        <a>To the archive</a>
      </Link>
    </div>
  )
}

export default StartPage;