import { AppProps } from "next/app"
import '../styling/global.css'

const MyApp = ({ Component, pageProps }: AppProps) => {
  return (
    <main>
      <Component {...pageProps} />
    </main>
  )
}

export default MyApp