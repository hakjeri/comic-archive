import { NextApiRequest, NextApiResponse } from "next"
import { CustomError } from "../../../utils/errors"
import { corsMiddleware } from "../../../utils/middleware"

const comicsHandler = async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    await corsMiddleware(req, res)
    const stripId = typeof req.query["id"] === "string"
      ? Number.parseInt(req.query["id"])
      : null
    if (!stripId) {
      throw new CustomError(400, "Requested comic ID is not a number")
    }
    const strip = await fetchStrip(stripId)
    res.status(200).json(strip)
  } catch (error) {
    res.status(error.statusCode || 500).send(error.message)
  }
}

const fetchStrip = async (index: number): Promise<Strip> => {
  try {
    const response = await fetch(`https://xkcd.com/${index}/info.0.json`)
    const { year, month, day, num, title, alt, img } = await response.json()
    return { publicationDate: new Date(year, month-1, day), num, title, alt, img}
  } catch (error) {
    throw error
  }
}

interface Strip {
  publicationDate: Date,
  alt: string,
  img: string,
  title: string,
  num: string
}

export default comicsHandler