import { NextApiRequest, NextApiResponse } from "next"
import { corsMiddleware } from "../../../utils/middleware"

const PAGINATION_AMOUNT = 12

const comicsHandler = async (req: NextApiRequest, res: NextApiResponse) => {
  await corsMiddleware(req, res)
  const stripIndex = typeof req.query["index"] === "string"
    ? Number.parseInt(req.query["index"])
    : null
  const response = await fetch('https://xkcd.com/info.0.json')
  const { num } = await response.json()
  const strips = await fetchStrips(stripIndex || num)
  res.status(200).json(strips)
}

const fetchStrip = async (index: number): Promise<Strip> => {
  try {
    const response = await fetch(`https://xkcd.com/${index}/info.0.json`)
    const { year, month, day, num, title, alt, img } = await response.json()
    return { publicationDate: new Date(year, month-1, day), num, title, alt, img}
  } catch (error) {
    throw error
  }
}

const fetchStrips = async (startIndex: number): Promise<(Strip)[]> => {
  const stripIndices = Array(startIndex < PAGINATION_AMOUNT
    ? startIndex
    : PAGINATION_AMOUNT
  ).fill(0);
  
  return await Promise.all(
    stripIndices.map((_, index: number) => fetchStrip(startIndex - index))
  )
}

interface Strip {
  publicationDate: Date,
  alt: string,
  img: string,
  title: string,
  num: string
}

export default comicsHandler