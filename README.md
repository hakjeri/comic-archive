# XKCD Comic archive

This repository contains a Next.js application used to browse previews and full comics from the https://xkcd.com/json.html API.

## Installation and setup

Requires, npm 6+, and node 12+.

To install dependencies, run `npm run install`. To run the development server, run `npm run dev`

## Project structure

This repository is setup as a classic next.js application written in TypeScript. This means that there is a an API under the `src/api` folder, and a server rendered react frontend with folder based routing defined under `src/pages`, and reusable components under `src/components`. There are also some utility functions found under `src/utils`.

Tailwind CSS is used as a base for styling.

The API has a middleware for allowing CORS requests.

## Deployment

The repository builds and deploys on netlify, under https://comic-archive.netlify.app.

## TODO

* Unit testing API
* Unit testing components and pages
* Docker deployment
* Open api documentation of endpoints
* Automatic linting/prettier on commit